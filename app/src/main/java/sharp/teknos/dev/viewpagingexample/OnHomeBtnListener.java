package sharp.teknos.dev.viewpagingexample;

public interface OnHomeBtnListener {

    void onSendBtnPressed(String title);
}
