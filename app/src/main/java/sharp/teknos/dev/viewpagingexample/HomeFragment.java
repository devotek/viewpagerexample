package sharp.teknos.dev.viewpagingexample;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment implements View.OnClickListener{

    public OnHomeBtnListener onHomeBtnListener;
    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (getActivity() instanceof OnHomeBtnListener){
            onHomeBtnListener = (OnHomeBtnListener) getActivity();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        Button button = (Button)view.findViewById(R.id.sendBtn);
        button.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) {
        if (onHomeBtnListener != null) {
            onHomeBtnListener.onSendBtnPressed("Send Btn Called");
        }
    }
}
